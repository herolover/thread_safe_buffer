#include <iostream>
#include <thread>
#include <atomic>

#include "thread_safe_buffer.h"

const int Mb = 1024 * 1024;
ThreadSafeBuffer buffer(5);
std::atomic_bool is_run(true);

void reader()
{
  while (is_run.load())
  {
    std::string message;
    if (buffer.read_until(message, "*"))
    {
      std::cout << "Message: " << message << std::endl;
    }
  }
}

int main()
{
  std::thread reader_thread(reader);

  while (!std::cin.eof())
  {
    std::string message;
    std::cin >> message;
    buffer.write(message);
  }

  is_run.store(false);
  reader_thread.join();

  return 0;
}

