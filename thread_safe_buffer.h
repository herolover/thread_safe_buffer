#ifndef THREAD_SAFE_BUFFER_H
#define THREAD_SAFE_BUFFER_H

#include <atomic>
#include <string>

class ThreadSafeBuffer
{
public:
  ThreadSafeBuffer(unsigned capacity);
  ~ThreadSafeBuffer();

  const char *data() const;
  unsigned size() const;
  int find(const std::string &str) const;
  bool read(char *data, unsigned size);
  bool read(std::string &data, unsigned size);
  bool read_until(std::string &data, const std::string &delimiter);
  bool ignore(unsigned size);
  void write(const char *data, unsigned size);
  void write(const std::string &data);
  void reset_if_possible();

private:
  std::atomic<char *> data_;
  unsigned capacity_;
  unsigned left_bound_;
  std::atomic_uint right_bound_;
};

#endif // THREAD_SAFE_BUFFER_H
