#ifndef THREAD_SAFE_QUEUE_H
#define THREAD_SAFE_QUEUE_H

#include <atomic>
#include <cstring>

#include "notifier.h"

template<class T>
class ThreadSafeQueue
{
public:
  inline
  ThreadSafeQueue(int capacity)
    : capacity_(capacity)
    , left_bound_(0)
    , right_bound_(0)
  {
    data_.store(new T [capacity_]);
  }

  inline
  ~ThreadSafeQueue()
  {
    delete[] data_;
  }

  inline
  void push(const T &value)
  {
    if (right_bound_ + 1 <= capacity_)
    {
      data_[right_bound_] = value;
      right_bound_ += 1;
      notifier_.notify();
    }
    else
    {
      unsigned new_capacity = capacity_ * 2;
      T *old_data = data_;
      T *new_data = new T [new_capacity];
      for (unsigned i = left_bound_; i < right_bound_; ++i)
      {
        new_data[i] = old_data[i];
      }
      new_data[right_bound_] = value;
      capacity_ = new_capacity;
      data_.store(new_data);
      right_bound_ += 1;
      notifier_.notify();
      delete[] old_data;
    }
  }

  inline
  T & front()
  {
    if (this->empty())
    {
      notifier_.wait();
    }

    return data_[left_bound_];
  }

  inline
  void pop()
  {
    if (this->empty())
    {
      notifier_.wait();
    }

    left_bound_ += 1;

    this->reset_if_possible();
  }

  inline
  bool empty()
  {
    return left_bound_ == right_bound_;
  }

  inline
  void reset_if_possible()
  {
    unsigned left_bound_tmp = left_bound_;
    if (right_bound_.compare_exchange_strong(left_bound_tmp, 0))
    {
      left_bound_ = 0;
    }
  }

private:
  std::atomic<T *> data_;
  unsigned capacity_;
  unsigned left_bound_;
  std::atomic<unsigned> right_bound_;
  Notifier notifier_;
};

#endif // THREAD_SAFE_QUEUE_H
