#ifndef NOTIFIER_H
#define NOTIFIER_H

#include <condition_variable>
#include <mutex>
#include <atomic>

class Notifier
{
public:
  inline
  void notify()
  {
    is_notifed_.store(true);
    cv_.notify_one();
  }

  inline
  void wait()
  {
    std::mutex mutex;
    std::unique_lock<std::mutex> lock(mutex);

    is_notifed_.store(false);

    cv_.wait(lock, [this]()
    {
      return is_notifed_.load();
    });
  }
private:
  std::condition_variable cv_;
  std::atomic<bool> is_notifed_;
};

#endif // NOTIFIER_H
