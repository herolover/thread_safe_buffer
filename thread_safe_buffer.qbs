import qbs 1.0

CppApplication {
    name: "thread_safe_buffer"

    files: [
        "main.cpp",
        "thread_safe_buffer.h",
        "thread_safe_buffer.cpp",
        "notifier.h",
        "thread_safe_queue.h"
    ]

    cpp.cxxFlags: [
        "-std=c++11"
    ]

    cpp.linkerFlags: [
        "-pthread"
    ]
}
