#include "thread_safe_buffer.h"

#include <cstring>

ThreadSafeBuffer::ThreadSafeBuffer(unsigned capacity)
  : capacity_(capacity)
  , left_bound_(0)
  , right_bound_(0)
{
  data_.store(new char [capacity_]);
}

ThreadSafeBuffer::~ThreadSafeBuffer()
{
  delete[] data_;
}

const char * ThreadSafeBuffer::data() const
{
  return data_ + left_bound_;
}

unsigned ThreadSafeBuffer::size() const
{
  return right_bound_ - left_bound_;
}

int ThreadSafeBuffer::find(const std::string &str) const
{
  int pos = -1;
  for (unsigned i = left_bound_; i < right_bound_; ++i)
  {
    bool found = true;
    for (unsigned j = 0; j < str.size(); ++j)
    {
      if (data_[i + j] != str[j])
      {
        found = false;
        break;
      }
    }

    if (found)
    {
      pos = i;
      break;
    }
  }

  return pos;
}

bool ThreadSafeBuffer::read(char *data, unsigned size)
{
  if (this->size() >= size)
  {
    std::memcpy(data, data_ + left_bound_, size);
    left_bound_ += size;

    this->reset_if_possible();

    return true;
  }

  return false;
}

bool ThreadSafeBuffer::read(std::string &data, unsigned size)
{
  if (this->size() >= size)
  {
    data.assign(data_ + left_bound_, size);
    left_bound_ += size;

    this->reset_if_possible();

    return true;
  }

  return false;
}

bool ThreadSafeBuffer::read_until(std::string &data, const std::string &delimiter)
{
  int pos = this->find(delimiter);
  if (pos != -1)
  {
    data.assign(data_ + left_bound_, pos - left_bound_);
    left_bound_ = pos + delimiter.size();

    this->reset_if_possible();

    return true;
  }

  return false;
}

bool ThreadSafeBuffer::ignore(unsigned size)
{
  if (this->size() >= size)
  {
    left_bound_ += size;

    this->reset_if_possible();

    return true;
  }

  return false;
}

void ThreadSafeBuffer::write(const char *data, unsigned size)
{
  if (right_bound_ + size <= capacity_)
  {
    std::memcpy(data_ + right_bound_, data, size);
    right_bound_ += size;
  }
  else
  {
    /*
     * Expand the buffer and copy data.
     */
    unsigned new_capacity = (right_bound_ + size) * 2;
    char *old_data = data_;
    char *new_data = new char [new_capacity];
    std::memcpy(new_data, old_data, capacity_);
    std::memcpy(new_data + right_bound_, data, size);
    capacity_ = new_capacity;
    data_.store(new_data);
    right_bound_ += size;
    delete[] old_data;
  }
}

void ThreadSafeBuffer::write(const std::string &data)
{
  this->write(data.c_str(), data.size());
}

/*
 * If left_bound_ is equal to right_bound then buffer is empty and we can set
 * both bounds to zero.
 */
void ThreadSafeBuffer::reset_if_possible()
{
  unsigned left_bound_tmp = left_bound_;
  if (right_bound_.compare_exchange_strong(left_bound_tmp, 0))
  {
    left_bound_ = 0;
  }
}
